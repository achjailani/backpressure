# First stage: Builder
FROM golang:1.20.7-alpine3.18 as builder

# Install build dependencies
RUN apk update && apk --no-cache add  build-base  git bash  coreutils openssh  openssl gettext

# Set the working directory inside the builder stage
WORKDIR /usr/src/app

COPY .env.docker .env
# Copy the source code into the builder stage
COPY . .

# Install build packages and perform the build
RUN go mod tidy && go mod download && go mod vendor
RUN go build -tags static_all -o foo

# Second stage: Runtime
FROM alpine:3.18.3

# Setup package dependencies
RUN apk --no-cache update  \
    && apk --no-cache add ca-certificates bash jq curl gettext

# Set the timezone
ENV TZ=Asia/Jakarta
RUN apk add -U tzdata && ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Set the working directory in the runtime stage
WORKDIR /opt/app

# Copy the built binary from the builder stage
COPY --from=builder /usr/src/app/foo .
COPY --from=builder /usr/src/app/.env .

# Set the command to run the application
CMD ["./foo"]