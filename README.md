# The Implemented of Backpressure
> Back Pressure (or backpressure) is the term for a resistance to the desired flow of fluid through pipes. [wikipedia](https://en.wikipedia.org/wiki/Back_pressure)

In the context of software, the definition could be tweaked to refer to the flow of data within software:

> Resistance or force opposing the desired flow of data through software. [jayphelps](https://medium.com/@jayphelps/backpressure-explained-the-flow-of-data-through-software-2350b3e77ce7)

### Analogy
Imagine, there is an employee named Katie who works in a cigarette factory, responsible for placing cigarette sticks into packs containing 12 sticks each. Katie can fill one pack in 30 seconds, while the cigarette machine processes 50 cigarette sticks per minute. What happens? Katie falls behind by 26 sticks every minute, or 13 sticks every 30 seconds.

Problem:
* Katie is overwhelmed with managing the cigarette packaging process.

Solution:
* Adding more packaging employees.
* Reducing the machine's speed.
* etc.


### Service Communication
Often in service communication (microservice, etc) we're not aware of what's going to happen when a server handles over requests.
#### Without Back Pressure
<p align="center">
  <img src="img2.jpg" />
</p>

#### With Back Pressure
<p align="center">
  <img src="img.jpg" />
</p>

### Implementation 
The implementation of Back Pressure on the project located at `interface/middleware/back_pressure.go`

### Docker 
Build
```shell
$ docker build . -t achjailani/backpressure
```

Run
```shell
$ docker run -it --rm --name backpressure -p 8181:8181 --env-file ./.env.docker achjailani/backpressure
```