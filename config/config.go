package config

import (
	"os"
	"strconv"
)

// Config is a struct of base config object
type Config struct {
	AppName         string
	AppPort         int
	AppEnv          string
	AppLang         string
	AppTimeZone     string
	TestMode        bool
	CacheConfig     CacheConfig
	CacheTestConfig CacheTestConfig
	BackRPS         int
}

// CacheConfig is a cache config for test
type CacheConfig struct {
	CacheHost     string
	CachePort     string
	CacheUsername string
	CachePassword string
	CacheDB       int
}

// CacheTestConfig is a cache config for test
type CacheTestConfig struct {
	CacheHost     string
	CachePort     string
	CacheUsername string
	CachePassword string
	CacheDB       int
}

// New is a function to initialize config
func New() *Config {
	return &Config{
		AppName:     getEnv("APP_NAME", ""),
		AppPort:     getEnvAsInt("APP_PORT", 8080),
		AppEnv:      getEnv("APP_ENV", "development"),
		AppLang:     getEnv("APP_LANG", "en"),
		AppTimeZone: getEnv("APP_TIMEZONE", ""),
		TestMode:    getEnvAsBool("TEST_MODE", false),
		CacheConfig: CacheConfig{
			CacheHost:     getEnv("CACHE_HOST", "127.0.0.1"),
			CachePort:     getEnv("CACHE_PORT", "6379"),
			CacheUsername: getEnv("CACHE_USERNAME", ""),
			CachePassword: getEnv("CACHE_PASSWORD", ""),
			CacheDB:       getEnvAsInt("CACHE_DB", 0),
		},
		CacheTestConfig: CacheTestConfig{
			CacheHost:     getEnv("TEST_CACHE_HOST", "127.0.0.1"),
			CachePort:     getEnv("TEST_CACHE_PORT", "6379"),
			CacheUsername: getEnv("TEST_CACHE_USERNAME", ""),
			CachePassword: getEnv("TEST_CACHE_PASSWORD", ""),
			CacheDB:       getEnvAsInt("TEST_CACHE_DB", 0),
		},
		BackRPS: getEnvAsInt("BACK_RPS", 0),
	}
}

// getEnv is a function to get env value as string
func getEnv(key string, defaultVal string) string {
	if value, exist := os.LookupEnv(key); exist {
		return value
	}

	if nextValue := os.Getenv(key); nextValue != "" {
		return nextValue
	}

	return defaultVal
}

// getEnvAsInt is a function to get env value as int
func getEnvAsInt(name string, defaultVal int) int {
	valueStr := getEnv(name, "")
	if value, err := strconv.Atoi(valueStr); err == nil {
		return value
	}

	return defaultVal
}

// getEnvAsBool is a function to get env value as bool
func getEnvAsBool(name string, defaultVal bool) bool {
	valueStr := getEnv(name, "")
	if value, err := strconv.ParseBool(valueStr); err == nil {
		return value
	}

	return defaultVal
}
