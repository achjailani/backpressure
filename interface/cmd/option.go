package cmd

// Option is an option type
type Option func(c *Command)
