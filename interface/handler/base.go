package handler

import "gitlab.com/achjailani/backpressure/config"

// Handler is a struct
type Handler struct {
	cfg *config.Config
}

// New is a constructor
func New(cfg *config.Config) *Handler {
	return &Handler{cfg: cfg}
}
