package handler

import (
	"fmt"
	"github.com/gin-gonic/gin"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/achjailani/backpressure/pkg/util"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	"gitlab.com/ugrd/orchestration-saga/package/response"
	"net/http"
	"time"
)

// Factorial is a method
func (h *Handler) Factorial(c *gin.Context) {
	var req FactorialRequest
	if err := c.ShouldBind(&req); err != nil {
		exception.NewHTTPError(c, err.Error()).
			WithHTTPStatus(http.StatusUnprocessableEntity).
			JSON()
		return
	}

	if err := req.Validate(); err != nil {
		fmt.Println(err)
		exception.NewHTTPError(c, fmt.Sprintf("Unprocessable Entity")).
			WithHTTPStatus(http.StatusUnprocessableEntity).
			WithErrFields(util.ErrToMap(err)).
			JSON()
		return
	}

	r := factorial(int64(req.Number))

	response.NewHTTPResponse(c, fmt.Sprintf("OK")).
		WithHTTPStatus(http.StatusOK).
		WithData(map[string]interface{}{
			"result": r,
		}).
		JSON()
	return
}

// FactorialRequest is a struct
type FactorialRequest struct {
	Number int `json:"number"`
}

// Validate is a method to validate payload
func (f FactorialRequest) Validate() error {
	return validation.ValidateStruct(&f,
		validation.Field(&f.Number, validation.Required),
	)
}

// factorial is a function to find the factor of given number
// @param number int
func factorial(number int64) int64 {
	var r int64
	r = 1

	for number != 0 {
		r = r * number
		number--
	}

	time.Sleep(30 * time.Millisecond)

	return r
}
