package handler

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/ugrd/orchestration-saga/package/response"
	"net/http"
)

func (h *Handler) Ping(c *gin.Context) {
	response.NewHTTPResponse(c, fmt.Sprintf("OK")).
		WithHTTPStatus(http.StatusOK).
		WithData(map[string]string{
			"message": "pong!",
		}).
		JSON()
	return
}
