package middleware

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/achjailani/backpressure/config"
	"gitlab.com/achjailani/backpressure/pkg/cache"
	"gitlab.com/ugrd/orchestration-saga/package/exception"
	"net/http"
	"strconv"
	"sync"
	"time"
)

// BackPressure is a struct
type BackPressure struct {
	cfg   *config.Config
	key   string
	cache cache.Cache
	mutex sync.Mutex
}

// NewBackPressure is a constructor
func NewBackPressure(cfg *config.Config, cache cache.Cache) *BackPressure {
	return &BackPressure{
		cfg:   cfg,
		key:   fmt.Sprintf("concurrent_number"),
		cache: cache,
	}
}

// Handle is a method to handle backpressure
func (bp *BackPressure) Handle() gin.HandlerFunc {
	return func(c *gin.Context) {
		bp.mutex.Lock()
		defer bp.mutex.Unlock()

		currSecond := time.Now().Second()
		cacheKey := fmt.Sprintf("%s:%d", bp.key, currSecond)

		_, err := bp.cache.Incr(c, cacheKey, 1)
		if err != nil {
			exception.NewHTTPError(c, fmt.Sprintf("Something went wrong!")).
				WithHTTPStatus(http.StatusInternalServerError).
				JSON()
			c.Abort()
			return
		}

		r, err := bp.cache.Get(c, cacheKey)
		if err != nil {
			exception.NewHTTPError(c, fmt.Sprintf("Something went wrong!")).
				WithHTTPStatus(http.StatusInternalServerError).
				JSON()
			c.Abort()
			return
		}
		num, _ := strconv.ParseInt(r.(string), 10, 0)

		if num > int64(bp.cfg.BackRPS) {
			exception.NewHTTPError(c, fmt.Sprintf("server is busy")).
				WithHTTPStatus(http.StatusServiceUnavailable).
				JSON()
			c.Abort()
			return
		}

		_ = bp.cache.Expire(c, cacheKey, time.Second)

		c.Next()
	}
}
