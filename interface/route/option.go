package route

import (
	"gitlab.com/achjailani/backpressure/config"
	"gitlab.com/achjailani/backpressure/pkg/cache"
)

// Option return Router with RouterOption to fill up the dependencies
type Option func(*Router)

// WithConfig is a function option
func WithConfig(cfg *config.Config) Option {
	return func(router *Router) {
		router.cfg = cfg
	}
}

// WithCache is a function option
func WithCache(cache cache.Cache) Option {
	return func(router *Router) {
		router.cache = cache
	}
}
