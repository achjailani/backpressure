package route

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/achjailani/backpressure/config"
	"gitlab.com/achjailani/backpressure/interface/handler"
	"gitlab.com/achjailani/backpressure/interface/middleware"
	"gitlab.com/achjailani/backpressure/pkg/cache"
)

// Router is a struct contains dependencies needed
type Router struct {
	cfg   *config.Config
	cache cache.Cache
}

// NewRouter is a constructor will initialize Router.
func NewRouter(options ...Option) *Router {
	router := &Router{}

	for _, opt := range options {
		opt(router)
	}

	return router
}

// Init is a function
func (r *Router) Init() *gin.Engine {
	gin.SetMode(gin.ReleaseMode)

	e := gin.Default()

	h := handler.New(r.cfg)
	backpressure := middleware.NewBackPressure(r.cfg, r.cache)

	e.Use(backpressure.Handle())

	e.GET("/ping", h.Ping)
	e.POST("/factorial", h.Factorial)

	return e
}
