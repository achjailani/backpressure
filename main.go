package main

import (
	"github.com/joho/godotenv"
	"github.com/urfave/cli/v2"
	"gitlab.com/achjailani/backpressure/config"
	"gitlab.com/achjailani/backpressure/interface/cmd"
	"gitlab.com/achjailani/backpressure/interface/route"
	"gitlab.com/achjailani/backpressure/pkg/cache"
	"gitlab.com/achjailani/backpressure/pkg/util"
	"log"
	"os"
	"strconv"
	"time"
)

// main is a function
func main() {
	if errEnv := godotenv.Load(); errEnv != nil {
		log.Fatal("Error loading .env file")
	}

	cfg := config.New()

	redis, err := cache.NewRedisCache(cfg)
	if err != nil {
		log.Fatalf("unable to connect to redis %v", err)
	}

	command := cmd.NewCommand()
	app := cmd.NewCLI()
	app.Commands = command.Build()

	app.Action = func(ctx *cli.Context) error {
		router := route.NewRouter(
			route.WithCache(cache.New(redis)),
			route.WithConfig(cfg),
		).Init()

		shutdownTimeout := 10 * time.Second

		er := util.RunHTTPServer(router, strconv.Itoa(cfg.AppPort), shutdownTimeout)
		if er != nil {
			return er
		}

		return nil
	}

	err = app.Run(os.Args)
	if err != nil {
		log.Fatalf("Unable to run CLI command, err: %v", err)
	}
}
