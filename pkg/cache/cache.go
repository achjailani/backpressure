package cache

import (
	"context"
	"time"
)

// Cache is an interface
type Cache interface {
	Get(ctx context.Context, key string) (interface{}, error)
	Set(ctx context.Context, key string, val interface{}, exp time.Duration) error
	Del(ctx context.Context, key string) error
	Expire(ctx context.Context, key string, exp time.Duration) error
	Incr(ctx context.Context, key string, val int64) (int64, error)
	Decr(ctx context.Context, key string, val int64) (int64, error)
}
