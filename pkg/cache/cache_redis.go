package cache

import (
	"context"
	"fmt"
	"github.com/redis/go-redis/v9"
	"gitlab.com/achjailani/backpressure/config"
	"time"
)

// RedisCache is a type
type RedisCache struct {
	cfg    *config.Config
	client *redis.Client
}

// Get is a method
func (r *RedisCache) Get(ctx context.Context, key string) (interface{}, error) {
	return r.client.Get(ctx, key).Result()
}

// Set is a method
func (r *RedisCache) Set(ctx context.Context, key string, val interface{}, exp time.Duration) error {
	return r.client.Set(ctx, key, val, exp).Err()
}

// Del is a method
func (r *RedisCache) Del(ctx context.Context, key string) error {
	return r.client.Del(ctx, key).Err()
}

// Incr is a method to increment value
func (r *RedisCache) Incr(ctx context.Context, key string, val int64) (int64, error) {
	return r.client.IncrBy(ctx, key, val).Result()
}

// Decr is a method to decrement value
func (r *RedisCache) Decr(ctx context.Context, key string, val int64) (int64, error) {
	return r.client.DecrBy(ctx, key, val).Result()
}

// Expire is a method to add expire time
func (r *RedisCache) Expire(ctx context.Context, key string, exp time.Duration) error {
	return r.client.Expire(ctx, key, exp).Err()
}

// NewRedisCache is a constructor
func NewRedisCache(cfg *config.Config) (*RedisCache, error) {
	opt := &redis.Options{
		Addr:     fmt.Sprintf("%s:%s", cfg.CacheConfig.CacheHost, cfg.CacheConfig.CachePort),
		Password: cfg.CacheConfig.CachePassword,
		DB:       cfg.CacheConfig.CacheDB,
	}

	if cfg.TestMode {
		opt = &redis.Options{
			Addr:     fmt.Sprintf("%s:%s", cfg.CacheTestConfig.CacheHost, cfg.CacheTestConfig.CachePort),
			Password: cfg.CacheTestConfig.CachePassword,
			DB:       cfg.CacheTestConfig.CacheDB,
		}
	}

	conn := redis.NewClient(opt)

	_, err := conn.Ping(context.Background()).Result()
	if err != nil {
		return nil, fmt.Errorf("failed to connect to redis: %v", err)
	}

	return &RedisCache{
		cfg:    cfg,
		client: conn,
	}, nil
}

var _ Cache = &RedisCache{}
