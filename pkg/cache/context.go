package cache

import (
	"context"
	"sync"
	"time"
)

const (
	// DefaultExpiration is a constant
	DefaultExpiration = 24 * time.Hour
)

// Context is a type
type Context struct {
	mtx   *sync.Mutex
	cache Cache
}

// New is a constructor
func New(cache Cache) *Context {
	impl := &Context{
		cache: cache,
		mtx:   &sync.Mutex{},
	}

	return impl
}

// Set is a method
func (c *Context) Set(ctx context.Context, key string, val interface{}, duration time.Duration) error {
	c.mtx.Lock()
	defer c.mtx.Unlock()

	return c.cache.Set(ctx, key, val, duration)
}

// Get is a method
func (c *Context) Get(ctx context.Context, key string) (interface{}, error) {
	return c.cache.Get(ctx, key)
}

// Del is a method
func (c *Context) Del(ctx context.Context, key string) error {
	return c.cache.Del(ctx, key)
}

// Expire is a method
func (c *Context) Expire(ctx context.Context, key string, exp time.Duration) error {
	return c.cache.Expire(ctx, key, exp)
}

// Incr is a method
func (c *Context) Incr(ctx context.Context, key string, val int64) (int64, error) {
	return c.cache.Incr(ctx, key, val)
}

// Decr is a method
func (c *Context) Decr(ctx context.Context, key string, val int64) (int64, error) {
	return c.cache.Decr(ctx, key, val)
}
